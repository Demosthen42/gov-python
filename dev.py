#!/usr/bin/env python3
from os import path
from app import create_app
from app.utils.route_helper import route_handler

def run():
    app = create_app()
    route_handler(app)
    app.run(host='192.168.33.10', port=5000, debug=True, use_reloader=True, threaded=True)


if __name__ == '__main__':
    run()