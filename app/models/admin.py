from . import Model

class Admin(Model):

    __table__ = 'admin'

    __timestamps__ = False

    __hidden__ = ['password']

    __fillable__ = [
        'email', 
        'password', 
        'phone', 
        'uuid', 
        'created_at'
    ]


    def get_jwt_identity(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'password': self.password,
            'role': 'admin'
        }