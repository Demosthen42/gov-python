from . import Model
from orator.orm import belongs_to_many

class Profile(Model):

    __table__ = 'profile'

    __timestamps__ = ['created_at', 'updated_at']

    __hidden__ = ['password']

    __fillable__ = [
        'email',
        'password',
        'name',
        'surname',
        'uuid',
    ]

    def get_jwt_identity(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'password': self.password,
            'role': 'client'
        }
    
    @belongs_to_many('user_law', 'user_id', 'law_id')
    def laws(self):
        from app.models.law import Law
        return Law