from orator.migrations import Migration


class CreateLawTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('law') as table:
            table.increments('id')
            table.string('name', 255)
            table.big_integer('nd').unique()
            table.timestamps()
 

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('law')