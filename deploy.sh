#!/usr/bin/env bash
# Asking for Bitbucket credentials
echo "Deploy script for GOV-python is running!"
# echo "Enter your GitLab username"
# read username
# echo "Enter your GitLab password"
# read password
# if [ -z "$username" ] || [ -z "$password" ]; then
# echo "Username or Password is Empty!"
# exit 0
# fi
# echo "Your username:password is $username:$password"

# Project folder name
project_folder=`pwd`
echo "Path to project folder - $project_folder"

# DB credentials
db_username=admin
db_password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
db_name=law
echo "DB_name - $db_name"
echo "DB_username - $db_username"
echo "DB_password - $db_password"

# Name for virtenv
virtenv_name=law
echo "virtenv_name - $virtenv_name"

# Installing all dependencies
sudo apt-get update -y;
sudo apt-get upgrade -y;
echo "All neded dependpencies will be installed!"
# Postgres repository
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main";
sudo wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - ;
# CertBOT repository
sudo add-apt-repository ppa:certbot/certbot -y;
# Installing majority of required packages
sudo apt-get update -y;
sudo apt-get install git postgresql-9.6 virtualenv virtualenvwrapper libpq-dev libffi-dev python3-dev build-essential libssl-dev supervisor nginx ufw memcached python-certbot-nginx -y;
# Installing NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash ;
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
source ~/.profile
nvm install 10.16.0
echo "All neded dependpencies installed!"

# Creating DB
echo "DB will be created!"
sudo su - postgres -c "psql -c \"CREATE USER $db_username WITH PASSWORD '$db_password';\""
sudo su - postgres -c "psql -c \" ALTER USER "$db_username" with SUPERUSER;\""
sudo su - postgres -c "psql -c \" ALTER USER "$db_username" with CREATEROLE;\""
sudo su - postgres -c "psql -c \" ALTER USER "$db_username" with CREATEDB;\""
sudo su - postgres -c "createdb -e -O $db_username $db_name"
echo "DB $db_name created! Owner $db_username"

# Creating virtualenv for python-backend
cd $project_folder
echo "virtualenv for python-backend will be created!"
echo "Creating virtualenv named $virtenv_name!"
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
echo "source /usr/share/virtualenvwrapper/virtualenvwrapper.sh" >> ~/.bashrc
export WORKON_HOME=~/.virtualenvs
mkdir $WORKON_HOME
echo "export WORKON_HOME=$WORKON_HOME" >> ~/.bashrc
echo "export PIP_VIRTUALENV_BASE=$WORKON_HOME" >> ~/.bashrc
source ~/.bashrc
export WORKON_HOME=~/.virtualenvs
mkvirtualenv -q -p /usr/bin/python3 $virtenv_name
deactivate
echo "virtualenv for python-backend created!"

# Deploying python-backend
echo "Deploying python-backend!"
echo "Installing requirements!"
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
cd $project_folder
workon $virtenv_name
pip install -r requirements.txt
deactivate

echo "Creating .env"
workon $virtenv_name
cd $project_folder
mkdir logs
rm $VIRTUAL_ENV/bin/postactivate
cp .env/default.postactivate .env/postactivate
sed -i -e 's/\r$//' .env/postactivate
ln -s `pwd`/.env/postactivate $VIRTUAL_ENV/bin/postactivate
rm $VIRTUAL_ENV/bin/predeactivate
cp .env/default.predeactivate .env/predeactivate
sed -i -e 's/\r$//' .env/predeactivate
ln -s `pwd`/.env/predeactivate $VIRTUAL_ENV/bin/predeactivate
deactivate

# Editing .env files
echo "Editing .env"
cd $project_folder
JWT_SECRET=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
sed -i 's/export DB_USER=.*/export DB_USER='"$db_username"'/'                       `pwd`/.env/postactivate
sed -i 's/export DB_PASSWORD=.*/export DB_PASSWORD='"$db_password"'/'               `pwd`/.env/postactivate
sed -i 's/export DB_NAME=.*/export DB_NAME='"$db_name"'/'                           `pwd`/.env/postactivate
sed -i 's+export PYTHONPATH=.*+export PYTHONPATH='"/$project_folder"'+'             `pwd`/.env/postactivate
sed -i 's+export LOG_PATH=.*+export LOG_PATH='"/$project_folder/logs"'+'            `pwd`/.env/postactivate
sed -i 's/export JWT_SECRET=.*/export JWT_SECRET='"$JWT_SECRET"'/'                  `pwd`/.env/postactivate

# App variables postactivate
echo ' '                                        >> `pwd`/.env/postactivate
echo '# App variables'                          >> `pwd`/.env/postactivate
echo 'export APP_HOST=127.0.0.1'                >> `pwd`/.env/postactivate
echo 'export APP_PORT=5000'                     >> `pwd`/.env/postactivate
echo 'export SERVICE_NAME=GOV'                  >> `pwd`/.env/postactivate
echo 'export APP_URL=http://localhost:8080/'    >> `pwd`/.env/postactivate
echo ' '                                        >> `pwd`/.env/postactivate

# App variables predeactivate
echo ' '                                        >> `pwd`/.env/predeactivate
echo '# App variables'                          >> `pwd`/.env/predeactivate
echo 'unset APP_HOST'                           >> `pwd`/.env/predeactivate
echo 'unset APP_PORT'                           >> `pwd`/.env/predeactivate
echo 'unset SERVICE_NAME'                       >> `pwd`/.env/predeactivate
echo 'unset APP_URL'                            >> `pwd`/.env/predeactivate
echo ' '                                        >> `pwd`/.env/predeactivate

# Creating DB migrations
echo "Migrations!"
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
cd $project_folder
workon $virtenv_name
python db.py migrate
deactivate

# Swap
sudo fallocate -l 2G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo cp /etc/fstab /etc/fstab.bak # backup
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

exit 0