import datetime
from os import environ
from flask import Flask, request
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_orator import Orator
from flask_jwt_extended import JWTManager
from app.utils.auth_helper import Auth
from pymemcache.client import base
from app.utils.response_helper import ResponseWrapper
from database.database_wrapper import config


app = Flask(__name__, static_folder='../public/dist/')
app.config['ORATOR_DATABASES'] = config

jwt_instance = JWTManager()
db = Orator(app)
flask_bcrypt = Bcrypt()
cache = base.Client(('localhost', 11211))

def create_app():
    app.config['JWT_SECRET_KEY'] = environ.get('JWT_SECRET')
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=7)
    app.response_class = ResponseWrapper

    flask_bcrypt.init_app(app)
    jwt_instance.init_app(app)
    cors = CORS(app)

    return app