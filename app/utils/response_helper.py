from flask import Response, jsonify

class ResponseWrapper(Response):
    @classmethod
    def force_type(cls, rv, environ=None):
        if isinstance(rv, dict):
            payload = {"data": rv}
            rv = jsonify(payload)
        return super(ResponseWrapper, cls).force_type(rv, environ)