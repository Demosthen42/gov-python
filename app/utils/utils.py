import json
import random
import string
import pprint
import requests
import logging
import phonenumbers
from phonenumbers import NumberParseException
from os import environ
from mailer import Mailer
from mailer import Message
from flask import Response
import hashlib


def debug(variable):
    pp = pprint.PrettyPrinter(indent=4, depth=6)
    debug_message = pp.pprint(variable)
    return Response(debug_message, mimetype="text/text")


def validate_phone(phone, locale=None):
    try:
        parsed_phone = phonenumbers.parse('+'+str(phone), locale)

        if phonenumbers.is_valid_number(parsed_phone):
            return int(phone)
        else:
            return None
    except NumberParseException as e:
        return None


def sms_code_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def generate_verification_sms(phone, code):
    text = _('Спасибо за регистрацию на сервисе %(service_name)s! Ваш проверочный код: %(code)s', 
        service_name=environ.get('SERVICE_NAME'), 
        code=code)
    sms = [
            {
                'phone': phone,
                'text': text,
                'channel': 'char',
                'sender': environ.get('P1SMS_SENDER')
            }
        ]
    return sms

def generate_recover_sms(phone, code):
    text = _('Код для восстановления пароля на сервисе %(service_name)s: %(code)s',
        service_name=environ.get('SERVICE_NAME'),
        code=code)
    sms = [
            {
                'phone': phone,
                'text': text,
                'channel': 'char',
                'sender': environ.get('P1SMS_SENDER')
            }
        ]
    return sms


def send_sms(sms, api_key=None):
    headers = {'accept': 'application/json', 'content-type': 'application/json'}
    params = {
        'apiKey': api_key if api_key else environ.get('P1SMS_APIKEY'),
        'webhookUrl': environ.get('APP_URL')+'/api/notification/hook_sms_status',
        'sms': sms
    }
    return requests.post(environ.get('P1SMS_ADMIN_PANEL') + environ.get('P1SMS_CREATE_PATH'),
                         data=json.dumps(params),
                         timeout=5,
                         headers=headers)

def send_email(to, subject, html, body=''):
    try:
        sender = Mailer(
            host=environ.get('SMTP_HOST'),
            port=int(environ.get('SMTP_PORT')),
            usr=environ.get('SMTP_LOGIN'),
            pwd=environ.get('SMTP_PASSWORD'),
            use_ssl=False
        )

        message = Message(From=environ.get('SMTP_SENDER_EMAIL'),
                          To=to,
                          charset="utf-8")
        message.Subject = subject
        message.Html = html
        if body:
            message.Body = body

        sender.send(message)
    except Exception as e:
        logging.error('Failed to send email {e}'.format(e=e))


def send_verification_email(user):
    try:
        hashstring = str.encode(str(user.id) + user.email)
        hash = hashlib.sha1(hashstring)
        url = environ.get('APP_URL') + '/registration-сomplete?uuid=' + user.uuid + '&hash=' + hash.hexdigest()

        subject = _('Подтвердите регистрацию на сервисе %(service_name)s',
            service_name=environ.get('SERVICE_NAME'))
        html = _("""Для завершения регистрации перейдите по ссылке: 
            <a href='%(url)s'>
                %(url)s
            </a>"""
            ,url=url)
        body = _('Для завершения регистрации перейдите по ссылке: %(url)s',url=url)

        send_email(user.email, subject, html, body)
    except Exception as e:
        logging.error('Failed to send verification email {e}'.format(e=e))

def send_code(email, code):
    sender = Mailer(
        host=environ.get('SMTP_HOST'),
        port=int(environ.get('SMTP_PORT')),
        usr=environ.get('SMTP_LOGIN'),
        pwd=environ.get('SMTP_PASSWORD'),
        use_ssl=False
    )
    message = Message(From=environ.get('SMTP_SENDER_EMAIL'),
                      To=email,
                      charset="utf-8")
    message.Subject = _("Востановление пароля на сервисе %(service_name)s", service_name=environ.get('SERVICE_NAME'))
    message.Body = _("Ваш код для восстановления доступа к сервису: %(code)s", code=code)

    sender.send(message)


def get_short_url(url):
    headers = {'accept': 'application/json', 'content-type': 'application/json'}
    params = {
        'apiKey': environ.get('SHORTENER_APIKEY'),
        'url': url
    }
    r = requests.post(environ.get('SHORTENER_PATH') + environ.get('SHORTENER_CREATE_PATH'),
                      data=json.dumps(params),
                      timeout=5,
                      headers=headers)
    return '{domain}/{hash}'.format(domain=environ.get('SHORTENER_DOMAIN'),
                                    hash=r.json()['hash'])


def send_push(devices, title, body, url, icon):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'key={fcm_key}'.format(fcm_key=environ.get('FCM_KEY'))
    }
    params = {
        "notification": {
            "title": title,
            "body": body,
            "click_action": url,
            "icon": icon
        },
        "registration_ids": devices
    }
    r = requests.post(environ.get('FCM_SEND_URL'),
                      data=json.dumps(params),
                      timeout=5,
                      headers=headers)
    return r.json()