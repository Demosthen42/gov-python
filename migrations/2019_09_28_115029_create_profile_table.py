from orator.migrations import Migration


class CreateProfileTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('profile') as table:
            table.increments('id')
            table.string('email', 255).unique()
            table.text('password')
            table.text('uuid').unique()
            table.string('name', 255).nullable()
            table.string('surname', 255).nullable()
            table.timestamp('created_at')
            table.timestamp('updated_at').nullable()
            table.text('token').nullable()
            table.text('refresh_token').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('profile')