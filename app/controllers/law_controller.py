import datetime
from flask import Blueprint, request, jsonify
from flask_jwt_extended import (get_jwt_identity, jwt_required)
from app.utils.auth_helper import admin_required

from app.models.law import Law

LawController = Blueprint('law', __name__)

@LawController.route('/get', methods=['GET'])
@jwt_required
def get():
    return jsonify(Law.order_by('id', 'asc').get().serialize()), 200

@LawController.route('/get/<id>', methods=['GET'])
@jwt_required
def get_by_id(id):
    return jsonify(Law.find(id).serialize()), 200