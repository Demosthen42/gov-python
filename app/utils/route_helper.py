import os
from .. import jwt_instance
from flask import send_from_directory, jsonify
from app.utils.auth_helper import Auth

from app.controllers.admin_controller import AdminController
from app.controllers.profile_controller import ProfileController
from app.controllers.law_controller import LawController


def register_blueprints(app):
    app.register_blueprint(AdminController, url_prefix='/api/admin')
    app.register_blueprint(ProfileController, url_prefix='/api/profile')
    app.register_blueprint(LawController, url_prefix='/api/law')


def route_handler(app):

    @jwt_instance.unauthorized_loader
    def unauthorized(callback):
        return Auth.unauthorized()

    # Handle react routes
    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def serve(path):
        if path != "" and os.path.exists(app.static_folder + path):
            return send_from_directory(app.static_folder, path)
        else:
            return send_from_directory(app.static_folder, 'index.html')

    register_blueprints(app)