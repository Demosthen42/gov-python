from functools import wraps
from flask import request, jsonify
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jwt_identity, verify_jwt_in_request, jwt_required)

from app.models.profile import Profile
from app.models.admin import Admin


class Auth:

    @staticmethod
    def unauthorized():
        return jsonify({
            'message': 'Missing Authorization Header'
        }), 401

    @staticmethod
    def refresh():
        current_user = get_jwt_identity()
        ret = {
            'token': create_access_token(identity=current_user)
        }
        return jsonify({'ok': True, 'data': ret}), 200

    @staticmethod
    def auth_user():
        user_uuid = get_jwt_identity()['uuid']
        return Profile.where('uuid', user_uuid).first_or_fail()

    @staticmethod
    def auth_admin():
        user_uuid = get_jwt_identity()['uuid']
        return Admin.where('uuid', user_uuid).first_or_fail()


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        identity = get_jwt_identity()
        if identity['role'] != 'admin':
            return jsonify({'message': 'Permission denied'}), 403
        else:
            return fn(*args, **kwargs)

    return wrapper
