from os import environ
from orator import DatabaseManager

config = {
    'pgsql': {
        'driver': environ.get('DB_DRIVER'),
        'host': environ.get('DB_HOST'),
        'database': environ.get('DB_NAME'),
        'user': environ.get('DB_USER'),
        'password': environ.get('DB_PASSWORD'),
        'prefix': ''
    }
}

class DatabaseWrapper(DatabaseManager):

    def __init__(self):
        super().__init__(config)