import uuid
import datetime
from app import flask_bcrypt
from app.models.admin import Admin



if __name__ == '__main__':
    email = input('Enter email: ')
    password = input('Enter password: ')
    try:
        password = flask_bcrypt.generate_password_hash(password).decode("utf-8")
        user_uuid = str(uuid.uuid4())

        Admin.create(email=email,
                    password=password,
                    uuid=user_uuid,
                    created_at=datetime.datetime.now())

        print('Admin created!')
    except Exception as e:
        print('Failed to create admin: {e}'.format(e=e))