# gov-python
python implementation 

# Deploy
1) Clone repo
    ``` bash
    cd ~
    git clone https://<user>:<password>@gitlab.com/gov-app/gov-python.git
    cd gov-python
    ```
2) Run deploy script
    ``` bash
    chmod +x ./deploy.sh
    ./deploy.sh
    sudo reboot
    ```
3) Create admin
    ``` bash
    workon law
    python ./app/script/create_admin.py
    ```
# Run
``` bash
workon law
python dev.py
```