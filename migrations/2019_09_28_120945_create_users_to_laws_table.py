from orator.migrations import Migration


class CreateUsersToLawsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('user_law') as table:
            table.big_integer('user_id')
            table.foreign('user_id').references('id').on('profile')
            table.big_integer('law_id')
            table.foreign('law_id').references('id').on('law')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('user_law')