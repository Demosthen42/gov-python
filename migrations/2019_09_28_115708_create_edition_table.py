from orator.migrations import Migration


class CreateEditionTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('edition') as table:
            table.increments('id')
            table.big_integer('law_id')
            table.foreign('law_id').references('id').on('law').on_delete('cascade')
            table.big_integer('rd').unique()
            table.string('date', 255)
            table.text('text')
            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('edition')