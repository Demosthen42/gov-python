import sys
import uuid
import time
import string
import logging
from .. import flask_bcrypt
from .. import cache
from app.utils.auth_helper import Auth, admin_required
from flask import Blueprint, request, jsonify, Response
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jwt_identity, jwt_required, jwt_refresh_token_required)

from app.models.admin import Admin

AdminController = Blueprint('admin', __name__)

@AdminController.route('/login', methods=['POST'])
def auth():
    data = request.get_json(force=True)
    user = Admin.where('email', data['email']).first_or_fail()

    if user and flask_bcrypt.check_password_hash(user.password, data['password']):
        access_token = create_access_token(identity=user.get_jwt_identity())
        refresh_token = create_refresh_token(identity=user.get_jwt_identity())

        user.token = access_token
        user.refresh_token = refresh_token
        user.save()

        return jsonify(user.to_dict()), 200

    else:
        return jsonify({'message': 'Wrong auth params'}), 400

@AdminController.route('/authuser', methods=['GET'])
@jwt_required
@admin_required
def auth_admin():
    time.sleep(2)
    return jsonify(Auth.auth_admin().serialize()), 200