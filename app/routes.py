from app.controllers.admin_controller import AdminController
from app.controllers.profile_controller import UserController
from app.controllers.law_controller import LawController

def register_blueprints(app):
    app.register_blueprint(AdminController, url_prefix='/api/admin')
    app.register_blueprint(UserController, url_prefix='/api/profile')
    app.register_blueprint(LawController, url_prefix='/api/laws')
