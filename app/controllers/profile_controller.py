import sys
import uuid
import time
import string
import logging
import threading
import math
from .. import flask_bcrypt
from .. import cache
from datetime import datetime
from app.utils.auth_helper import Auth
from app.utils.auth_helper import admin_required
from flask import Blueprint, request, jsonify, Response
from email_validator import validate_email as val_e, EmailNotValidError
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jwt_identity, jwt_required, jwt_refresh_token_required, decode_token)
from app.models.profile import Profile
import hashlib

ProfileController = Blueprint('profile', __name__)

@ProfileController.route('/register', methods=['POST'])
def register():
    data = request.get_json()

    password = flask_bcrypt.generate_password_hash(data['password']).decode("utf-8")

    try:
        v = val_e(data['email'])
    except EmailNotValidError:
        return jsonify({'message': 'Email not valid'}), 400

    if (Profile.where('email', data['email']).first()):
        return jsonify({'message': 'User with this email is already registered'}), 400

    user_uuid = str(uuid.uuid4())

    user = Profile.create(email=data['email'],
                            password=password,
                            name='',
                            uuid=user_uuid,)

    return jsonify({'uuid': user.uuid})

@ProfileController.route('/login', methods=['POST'])
def auth():
    data = request.get_json(force=True)
    email = data['email']
    try:
        v = val_e(email)
    except EmailNotValidError:
        return jsonify({'message': 'Email not valid'}), 400

    user = Profile.where('email', email).first_or_fail()

    if email and user and flask_bcrypt.check_password_hash(user.password, data['password']):
        access_token = create_access_token(identity=user.get_jwt_identity())
        refresh_token = create_refresh_token(identity=user.get_jwt_identity())

        user.token = access_token
        user.refresh_token = refresh_token
        user.save()

        return jsonify(user.to_dict()), 200

    else:
        return jsonify({'message': 'Wrong auth params'}), 400

@ProfileController.route('/loginbytoken', methods=['POST'])
def auth_by_token():
    data = request.get_json(force=True)
    token = data['token']

    if not token:
        return jsonify({'message': 'Empty token'}), 400

    decoded_token = decode_token(token)
    current_user = decoded_token['identity']
    user = Profile.where('uuid', current_user['uuid']).first_or_fail()

    return jsonify(user.to_dict()), 200

@ProfileController.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    ret = {
        'access_token': create_access_token(identity=current_user)
    }
    return jsonify(ret), 200

@ProfileController.route('/authuser', methods=['GET'])
@jwt_required
def users():
    return jsonify(Auth.auth_user().serialize()), 200

@ProfileController.route('/change_password', methods=['POST'])
@jwt_required
def change_password():
    user = Auth.auth_user()
    password = request.get_json(force=True)['password']
    if password:
        hashed_password = flask_bcrypt.generate_password_hash(password).decode("utf-8")
        user.password = hashed_password
        user.save()
        return '', 200
    else:
        return jsonify({'message': 'Password field is empty'})

@ProfileController.route('/update', methods=['POST'])
@jwt_required
def update_profile():
    user = Auth.auth_user()
    data = request.get_json(force=True)

    if data['email']:
        try:
            v = val_e(data['email'])
        except EmailNotValidError:
            return jsonify({'message': 'Email not valid'}), 400
        user.email = data['email']
    if data['name']:
        user.name = data['name']
    if data['surname']:
        user.surname = data['surname']
    user.save()

    return jsonify({'user': user.serialize()}), 200

@ProfileController.route('/create_new_password', methods=['POST'])
def create_new_password():
    email = request.get_json(force=True)['email']
    try:
        user = Profile.where('email', email).first_or_fail()
    except Exception:
        return jsonify({'message': 'User not found'}), 400
    password = request.get_json(force=True)['password']
    if password:
        hashed_password = flask_bcrypt.generate_password_hash(password).decode("utf-8")
        user.password = hashed_password
        user.save()
        return jsonify({'message': 'OK'}), 200
    else:
        return jsonify({'message': 'Password field is empty'}), 400