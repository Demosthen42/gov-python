from orator import Model
from database.database_wrapper import DatabaseWrapper

Model.set_connection_resolver(DatabaseWrapper())