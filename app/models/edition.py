from . import Model
from orator.orm import has_many

class Edition(Model):

    __table__ = 'edition'

    __timestamps__ = ['created_at', 'updated_at']

    __fillable__ = [
        'law_id',
        'rd',
        'date',
        'text'
    ]

    @has_many('law_id')
    def law(self):
        from app.models.law import Law
        return Law