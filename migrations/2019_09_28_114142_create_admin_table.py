from orator.migrations import Migration


class CreateAdminTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('admin') as table:
            table.increments('id')
            table.string('email', 255).unique()
            table.text('password')
            table.text('uuid').unique()
            table.timestamp('created_at')
            table.text('token').nullable()
            table.text('refresh_token').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('admin')
